import datetime
import uuid

from loguru import logger
from telegram import Message

import db


def add_event(event_type: str, received_message_chat_id: str, received_message_id: str, forwarded_message_id: str):
    id = str(uuid.uuid4())
    db.execute(
        "INSERT INTO events VALUES (?, ?, ?, ?, ?, ?)",
        (id, event_type, received_message_chat_id, received_message_id, forwarded_message_id,
         datetime.datetime.now()),
    )


def message_forwarded(received_message: Message, forwarded_message: Message):
    logger.info("Forwarded message = {}", forwarded_message)
    add_event("message_forwarded", received_message.chat.id, received_message.message_id, forwarded_message.message_id)


def find_feedback(forwarded_message_id):
    res = db.fetch_one(
        "SELECT * FROM events WHERE type = 'message_forwarded' AND forwarded_message_id = ?",
        (str(forwarded_message_id),),
    )
    return {
        'type': res[1],
        'received_message_chat_id': res[2],
        'received_message_id': res[3],
        'forwarded_message_id': res[4],
    }
