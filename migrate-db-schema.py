import db


def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Exception as e:
        print(e)


def main():
    sql_table = """CREATE TABLE IF NOT EXISTS events (
    id VARCHAR(36) NOT NULL,
    type VARCHAR(255),
    received_message_chat_id VARCHAR(255),
    received_message_id VARCHAR(255),
    forwarded_message_id VARCHAR(255),
    collected_at timestamp with time zone,
    PRIMARY KEY (id)
);"""

    # create a database connection
    conn = db.connect()

    # create tables
    if conn is not None:
        # create projects table
        create_table(conn, sql_table)
    else:
        print("Error! cannot create the database connection.")


main()
