FROM python:3-alpine

RUN apk update \
  && apk add --virtual build-deps gcc python3-dev musl-dev \
  && apk add postgresql-dev
RUN pip install python-telegram-bot loguru psycopg2-binary pytest
RUN apk del build-deps

WORKDIR /bot
COPY ./* /bot/

CMD ["python", "main.py"]

