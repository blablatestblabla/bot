import logging
from typing import Dict

from telegram import Update, InlineKeyboardButton, \
    InlineKeyboardMarkup, ReplyKeyboardMarkup
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext, CallbackQueryHandler,
)

# Enable logging
from config import TG_TOKEN, WELCOME_MESSAGE, CHAT_ID, NEXT_MESSAGE, BACK_MESSAGE, GO_TO_START_BUTTON_TEXT
from handlers.support.support_handlers import forward_callback, reply_callback

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

END = ConversationHandler.END
CHOOSING, TYPING_REPLY, TYPING_CHOICE = range(3)
BACK_OPTION, NEXT_OPTION, SUPPORT_OPTION = map(chr, range(4, 7))
CHAT_WITH_OPERATOR, GO_TO_START = map(chr, range(10, 12))


def facts_to_str(user_data: Dict[str, str]) -> str:
    """Helper function for formatting the gathered user info."""
    facts = [f'{key} - {value}' for key, value in user_data.items()]
    return "\n".join(facts).join(['\n', '\n'])


def show_start_buttons(update: Update, context: CallbackContext, *, text: str):
    buttons = [[
        InlineKeyboardButton(text='Назад', callback_data=str(BACK_OPTION)),
        InlineKeyboardButton(text='Вперед', callback_data=str(NEXT_OPTION)),
        InlineKeyboardButton(text='Вызов оператора', callback_data=str(SUPPORT_OPTION)),
    ]]
    keyboard = InlineKeyboardMarkup(buttons)
    update.effective_message.reply_text(text=text, reply_markup=keyboard)


def start(update: Update, context: CallbackContext) -> int:
    show_start_buttons(update, context, text=WELCOME_MESSAGE)
    return CHOOSING


def end_second_level(update, context):
    """Return to top level conversation."""
    start(update, context)
    return END


# Error handler
def error(update, context):
    """Log Errors caused by Updates."""
    logger.exception('Update "%s" caused error "%s"' % (str(update), str(context.error)))
    update.effective_message.bot.send_message(chat_id=CHAT_ID, text='Ошибка в боте:' + str(context.error))


def start_chat_with_operator_callback(update: Update, context: CallbackContext):
    reply_keyboard = [
        [GO_TO_START_BUTTON_TEXT],
    ]
    keyboard = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True)
    update.effective_message.reply_text(text='Напишите ваш запрос помощнику.', reply_markup=keyboard)
    return CHAT_WITH_OPERATOR


def next_callback(update: Update, context: CallbackContext):
    show_start_buttons(update, context, text=NEXT_MESSAGE)
    return END


def back_callback(update: Update, context: CallbackContext):
    show_start_buttons(update, context, text=BACK_MESSAGE)
    return END


def end_chat_callback(update: Update, context: CallbackContext):
    show_start_buttons(update, context, text='Чат окончен')
    return END


def reply_wrapper_callback(update: Update, context: CallbackContext):
    reply_callback(update, context)


def forward_wrapper_callback(update: Update, context: CallbackContext):
    forward_callback(update, context)


def main() -> None:
    """Run the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater(TG_TOKEN, use_context=True, workers=50)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    support_conversation_handler = ConversationHandler(
        entry_points=[CallbackQueryHandler(start_chat_with_operator_callback,
                                           pattern='^' + str(SUPPORT_OPTION) + '$')],

        states={
            CHAT_WITH_OPERATOR: [
                MessageHandler(Filters.regex(GO_TO_START_BUTTON_TEXT), end_chat_callback),
                MessageHandler(
                    Filters.text, reply_wrapper_callback
                ),
                MessageHandler(
                    Filters.all, forward_wrapper_callback
                ),
                CallbackQueryHandler(end_chat_callback, pattern='^' + str(GO_TO_START) + '$')
            ],
        },

        fallbacks=[
            CallbackQueryHandler(start)
        ],

        map_to_parent={
            END: CHOOSING,
        }
    )

    next_conversation_handler = ConversationHandler(
        entry_points=[CallbackQueryHandler(next_callback, pattern='^' + str(NEXT_OPTION) + '$')],

        states={},

        fallbacks=[],

        map_to_parent={
            END: CHOOSING,
        }
    )

    back_conversation_handler = ConversationHandler(
        entry_points=[CallbackQueryHandler(back_callback, pattern='^' + str(BACK_OPTION) + '$')],

        states={},

        fallbacks=[],

        map_to_parent={
            END: CHOOSING,
        }
    )

    # Add conversation handler with the states CHOOSING, TYPING_CHOICE and TYPING_REPLY
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            CHOOSING: [
                support_conversation_handler,
                next_conversation_handler,
                back_conversation_handler,
            ],
        },
        fallbacks=[],
    )

    dispatcher.add_handler(conv_handler)

    dispatcher.add_handler(MessageHandler(Filters.reply, reply_callback))

    dispatcher.add_error_handler(error)
    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
