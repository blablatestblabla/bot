import sqlite3
from typing import Tuple

from config import DB_URI


def connect():
    conn = sqlite3.connect(DB_URI)
    return conn


def execute(sql: str, params: Tuple):
    with connect() as conn:
        cursor = conn.cursor()
        return cursor.execute(sql, params)


def fetch_one(sql: str, params: Tuple = None):
    with connect() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, params)
        return cursor.fetchone()
