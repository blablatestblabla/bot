import os

TG_TOKEN = os.environ.get("TG_BOT_TOKEN")
CHAT_ID = int(
    os.environ.get("CHAT_ID")
)  # The chat group or individual chat id of forwarded message destination
BOT_TIMEOUT = 10
BOT_RETRIES = 5
WELCOME_MESSAGE = os.environ.get("WELCOME_MESSAGE", "Hello")
NEXT_MESSAGE = os.environ.get("NEXT_MESSAGE", "Next")
BACK_MESSAGE = os.environ.get("BACK_MESSAGE", "Back")
HELP_MESSAGE = os.environ.get("HELP_MESSAGE", "DEMO Author: William Lee")
GO_TO_START_BUTTON_TEXT = 'Вернуться к инструкциям'

DB_URI = os.environ.get("DB_FILE_PATH")
